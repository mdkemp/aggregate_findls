#!/usr/bin/env python3

import re
import sys

def longest_common(str1, str2):
    for i in range(min(len(str1), len(str2))):
        if str1[i] != str2[i]:
            break
        c=i
    return str1[:c+1]

def auto_bkmg(size):
    if size >= 1024**3:
        return str(size*1.0/1024**3)+' GB'
    elif size >= 1024**2:
        return str(size*1.0/1024**2)+' MB'
    elif size >= 1024:
        return str(size*1.0/1024)+' KB'
    else:
        return str(size) + ' B'

# Begin main body

SPACE_TOKEN='%%SPACETOKEN%%'

filecount = 0
dircount = 0
linkcount = 0
othercount = 0
commonprefix=''
totalsize = 0

for line in sys.stdin:
    # Clean and normalize spaces in filenames
    line=line.strip()
    line=line.replace(r'\ ', SPACE_TOKEN)
    line=line.replace(r' -> ', SPACE_TOKEN + '->' + SPACE_TOKEN)

    # Split into components
    # 4883      4 -rwxr-xr-x   1 mdkemp   mdkemp         91 Mar 23 11:08 ./aggregate_findls.py
    match=re.search(r'^\d+\s+\d+\s+(?P<perms>\S+).*?(?P<size>\d+)\s+(?P<date>([A-Za-z]{3}\s+\d+\s+[0-9:]+\s+))(?P<filename>\S+)$', line)
    if not match:
        print('ERROR: Unable to parse "' + line + '"')
        sys.exit(1)

    perms = match.group('perms')
    size = match.group('size')
    filename=match.group('filename')

    if perms.startswith('d'):
        dircount += 1
    elif perms.startswith('l'):
        linkcount += 1
    elif perms.startswith('-'):
        filecount += 1
    else:
        othercount += 1

    totalsize += int(size)

    # Find longest common prefix
    if filename != '.':
        if commonprefix == '':
            commonprefix = filename
        commonprefix = longest_common(commonprefix, filename)
    

# Print stats
print('TotalSize:', auto_bkmg(totalsize))
print('Files:', filecount)
print('Dirs:', dircount)
print('Links:', linkcount)
print('Other:', othercount)
print('TotalItems:', filecount + dircount + linkcount + othercount)
print('CommonPrefix:', commonprefix)
    

